﻿using IntroToPrism.ViewModels;
using IntroToPrism.Views;
using Prism;
using Prism.Ioc;
using Prism.Unity;
using Xamarin.Forms;

namespace IntroToPrism
{
    public partial class App : PrismApplication
    {
        public App(IPlatformInitializer initializer = null) : base(initializer)
        {
            
        }

        protected override void OnInitialized()
        {
            InitializeComponent();

            NavigationService.NavigateAsync(nameof(IntroToPrismPage));
        }

        protected override void RegisterTypes(Prism.Ioc.IContainerRegistry containerRegistery)
        {
            containerRegistery.RegisterForNavigation<IntroToPrismPage, IntroToPrismPageViewModel>();
            containerRegistery.RegisterForNavigation<Page1, Page1ViewModel>();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
