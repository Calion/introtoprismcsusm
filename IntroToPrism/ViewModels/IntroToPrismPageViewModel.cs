﻿using System;
using System.Diagnostics;
using IntroToPrism.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;

namespace IntroToPrism.ViewModels
{
    public class IntroToPrismPageViewModel : BindableBase
    {

        public DelegateCommand NavigateCommand { get; set; }
        private readonly INavigationService _navigationService;

        public IntroToPrismPageViewModel(INavigationService navigationService)
        {
            Debug.WriteLine($"****{this.GetType().Name}: ctor");
            _navigationService = navigationService;
            NavigateCommand = new DelegateCommand(Navigate);
        }

        private void Navigate()
        {
            Debug.WriteLine("OnClickButtonNavigation");
            _navigationService.NavigateAsync($"{nameof(Page1)}");
        }
    }
}