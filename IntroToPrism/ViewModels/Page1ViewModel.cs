﻿using System;
using System.Diagnostics;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;

namespace IntroToPrism.ViewModels
{
    public class Page1ViewModel : BindableBase
    {
        public DelegateCommand NavigateCommand { get; set; }
        private readonly INavigationService _navigationService;

        public Page1ViewModel(INavigationService navigationService)
        {
            Debug.WriteLine($"****{this.GetType().Name}: ctor");
            _navigationService = navigationService;
            NavigateCommand = new DelegateCommand(GoBack);
        }

        private void GoBack()
        {
            _navigationService.GoBackAsync();
        }
    }
}